<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{

    public function testLoginExist()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');

        $button = $crawler->selectButton('buttonLogin');
        // select the form that contains this button
        $form = $button->form();

        $client->submit($form,[
            'email' => 'sixtine@gmail.com',
            'password' => 'AZERTY',
         ]);

        $this->assertSelectorTextContains('h1', 'Home');

    }

    public function testLoginNoExist()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');

        $button = $crawler->selectButton('buttonLogin');
        // select the form that contains this button
        $form = $button->form();

        $client->submit($form,[
            'email' => 'imposteur@gmail.com',
            'password' => 'AZERTY',
        ]);
        $this->assertSelectorTextContains('.alert', 'Email could not be found.');
    }


        /**
     * @dataProvider userLogin
     * @param mixed $email
     * @param mixed $password
     * @param mixed $result
     * @return void
     */
    public function testLogin($email, $password, $result)
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');
        $button = $crawler->selectButton('buttonLogin');
        // select the form that contains this button
        $form = $button->form();
        $this->userLogin();
        $client->submit($form,[
            'email' => $email,
            'password' => $password,
        ]);


        $this->assertSelectorTextContains('body', $result);


    }
    /**
     *  Data provider for : testLogin()
     * @return array
     */
    public function userLogin() {
        return [
            ['email' => 'sixtine@gmail.com', 'password' => 'AZERTY', 'result' => 'Home'],
            ['email' => 'imposteur@gmail.com', 'password' => 'AZERTY', 'result' => 'Email could not be found.']
        ];
    }
}


