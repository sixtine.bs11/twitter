<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

     /**
     * @return Message[] Returns an array of Message objects
     */
// create query for recover user and msg in database and join them together
    public function msgJoinUser()
    {
        $qd = $this->createQueryBuilder('m')
            ->innerJoin('m.user', 'u')

            ->orderBy('m.id', 'DESC')
            ->setMaxResults(10);
            // recover the query and result from the query
            return $qd->getQuery()->getResult()
            ;
    }

    public function tagJoinMsg()
    {
    $qd = $this->createQueryBuilder('h')
        //->andWhere('u.messages')
        ->innerJoin('h.hastags', 'hastags');


        return $qd ->getQuery()->getResult()
    ;
    }

}
