<?php

namespace App\Repository;

use App\Entity\Hastag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Hastag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hastag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hastag[]    findAll()
 * @method Hastag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HastagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hastag::class);
    }

     /**
     * @return Hastag Returns an array of Hastag objects
    */

    public function findOneHastag($value)
    {
        // recover an array
        $qd = $this->createQueryBuilder('t')
            ->where('t.tag = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
        return $qd;
        ;
    }


}
