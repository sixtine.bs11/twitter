<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $msg;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages")
     */
    private $user;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hastag", mappedBy="hastag")
     */
    private $hastags;

    public function __construct()
    {
        $this->hastags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMsg(): ?string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getMsg();
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Hastag[]
     */
    public function getHastags(): Collection
    {
        return $this->hastags;
    }

    public function addHastag(Hastag $hastag): self
    {
        if (!$this->hastags->contains($hastag)) {
            $this->hastags[] = $hastag;
            $hastag->addHastag($this);
        }

        return $this;
    }

    public function removeHastag(Hastag $hastag): self
    {
        if ($this->hastags->contains($hastag)) {
            $this->hastags->removeElement($hastag);
            $hastag->removeHastag($this);
        }

        return $this;
    }
}
