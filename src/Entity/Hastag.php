<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HastagRepository")
 */
class Hastag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    // C'est un message qui est relié au tag
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Message", inversedBy="hastags")
     */
    private $hastag;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $tag;

    public function __construct()
    {
        $this->hastag = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Message[]
     */
    public function getHastag(): Collection
    {
        return $this->hastag;
    }

    public function addHastag(Message $hastag): self
    {
        if (!$this->hastag->contains($hastag)) {
            $this->hastag[] = $hastag;
        }

        return $this;
    }

    public function removeHastag(Message $hastag): self
    {
        if ($this->hastag->contains($hastag)) {
            $this->hastag->removeElement($hastag);
        }

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(?string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }
}
