<?php

namespace App\Controller;



use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

class FollowerController extends AbstractController
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManagerInterface;



    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManagerInterface)
    {
        $this->userRepository = $userRepository;
        $this->entityManagerInterface = $entityManagerInterface;

    }

    /**
     * @Route("/follower/{user_id}", name="follower")
     */

    public function followMe(int $user_id)
    {
        //recover The User
        $Me = $this->getUser();
        // recover the user_id of the page
        $userFollow = $this->userRepository->find($user_id);
        // add the user_id in user(me) follower
        $userFollow->addFollower($Me);
        $this->entityManagerInterface->persist($userFollow);
        $this->entityManagerInterface->persist($Me);
        $this->entityManagerInterface->flush();

        return $this->redirectToRoute('profile', [
            'id' => $user_id
        ]);
    }

}
