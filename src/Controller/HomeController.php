<?php

namespace App\Controller;

use App\Entity\Hastag;
use App\Entity\Message;
use App\Repository\HastagRepository;
use App\Repository\MessageRepository;
use App\Form\HomeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    private $entityManager;
    private $messageRepository;
    private $hastagRepository;

    /**
     * HomeController constructor.
     * @param EntityManagerInterface $entityManager
     * @param MessageRepository $messageRepository
     * @param HastagRepository $hastagRepository
     */
    public function __construct(EntityManagerInterface $entityManager, MessageRepository $messageRepository, HastagRepository $hastagRepository)
    {
        $this->entityManager = $entityManager;
        $this->messageRepository = $messageRepository;
        $this->hastagRepository = $hastagRepository;
    }

    /**
     * @Route("/home", name="home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */

    public function message(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $msg = new Message();
        //recover the user
        $user = $this->getUser();
        // send msg to the user
        $msg->setUser($user);
        $form = $this->createForm(HomeType::class, $msg);
        //pour traiter les données du formulaire
        $form->handleRequest($request);

        $tweets = $this->messageRepository->msgJoinUser();
        $getTag = $this->messageRepository->tagJoinMsg();

        // for send a msg when is submit ans valid, and submit a hashtag if exist
         if ($form->isSubmitted() && $form->isValid()) {

             // get() : Returns a parameter by name. getData()return an array => for recover the hastag field
             $tagfrom = $form->get('hastag')->getData();

             //cut the string after the #
            $word = explode('#',$tagfrom);
            // for see the information
            dump($word);

             //loop for read the array
            foreach($word as $index => $key){
                if($index>0){
                    // delete the space between the word
                    $trimKey = trim($key);
                    // search in Db if the word existe
                    $tagDb = $this->hastagRepository->findOneHastag($trimKey);
                    dump($tagDb);

                    // If more than 0 add the tag in the message
                    if (!empty($tagDb)){
                        $msg->addHastag($tagDb);
                        // if not creat in the DB set tag and add the msg
                    }else{
                        $tag = new Hastag();
                        $tag->setTag($trimKey);
                        dump($tag);
                        $msg->addHastag($tag);

                        $this->entityManager->persist($tag);
                    }
                }
            }
             // send the Date time
             $msg->setDate(new \DateTime());
             $this->entityManager->persist($msg);
             $this->entityManager->flush();


              return  $this->render('home/index.html.twig',[
                  'form' => $form->createView(),
                  'tweets' => $tweets,
                  'user' => $user,
                  'tags' => $getTag,
              ]);

        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
            'tweets' => $tweets,
            'user' => $user,
            'tags' => $getTag,
        ]);
    }

}
