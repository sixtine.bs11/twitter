<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;

class ProfileController extends AbstractController
{
    private $messageRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(MessageRepository $messageRepository, UserRepository $userRepository)
    {
        $this->messageRepository = $messageRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/profile/{id}", name="profile")
     */
    public function index(int $id)
    {
        // recover the user and message
        $userProfile = $this->userRepository->find($id);
        $msgs = $userProfile->getMessages();

        $arrayFollower = $userProfile->getFollowers()->toArray();

        $arrayFollowing = $userProfile->getFollowing()->toArray();

        return $this->render('profile/index.html.twig', [
            'user' => $userProfile,
            'msgs' => $msgs,
            'followers' => $arrayFollower,
            'followings' => $arrayFollowing,

        ]);
    }
}
